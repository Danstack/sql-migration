CREATE PROCEDURE GetLosses @LineId INT, @From DATETIME, @To DATETIME
AS
BEGIN
    SELECT
        b.[LineId]
        ,b.[StoppageReasonCategory]
        ,b.[StoppageReasonCode]
        ,SUM(b.InstanceStart) + SUM(b.InstanceEarliestRecord) AS InstancesCount
        ,SUM(b.Duration) + SUM(b.DurationFromFrom) AS Duration
    FROM
    (SELECT
        a.*
        ,LEAD([DateTime],1,NULL) OVER (PARTITION BY [LineId] ORDER BY [DateTime] ASC) AS Lead_DateTime -- this is only used for dev/test
        ,CASE WHEN InstanceStart=1 THEN DATEDIFF(SECOND, [DateTime], LEAD([DateTime],1,NULL) OVER (PARTITION BY [LineId] ORDER BY [DateTime] ASC)) ELSE 0 END AS Duration
        ,CASE WHEN InstanceEarliestRecord=1 THEN DATEDIFF(SECOND, [DateTime], LEAD([DateTime],1,NULL) OVER (PARTITION BY [LineId] ORDER BY [DateTime] ASC)) ELSE 0 END AS DurationFromFrom
    FROM
        (SELECT
            [LineId]
            ,[DateTime]
            ,[Stoppage]
            ,[StoppageReasonCategory]
            ,[StoppageReasonCode]
            ,LEAD(Stoppage,1,NULL) OVER (PARTITION BY [LineId] ORDER BY [DateTime] ASC) AS Lead_Stoppage -- this is only used for dev/test
            ,CASE WHEN ([Stoppage]=0 AND (LEAD(Stoppage,1,NULL) OVER (PARTITION BY [LineId] ORDER BY [DateTime] ASC))=1) THEN 1 ELSE 0 END AS InstanceStart
            ,CASE WHEN ([Stoppage]=1 AND (LEAD(Stoppage,1,NULL) OVER (PARTITION BY [LineId] ORDER BY [DateTime] ASC))=0) THEN 1 ELSE 0 END AS InstanceEnd
            ,CASE WHEN ROW_NUMBER() OVER (PARTITION BY [LineId] ORDER BY [DateTime] ASC) = 1 THEN 1 ELSE 0 END AS EarliestRecord -- flag the earliest record returned
            ,CASE WHEN (ROW_NUMBER() OVER (PARTITION BY [LineId] ORDER BY [DateTime] ASC)=1) AND ([Stoppage]=1) THEN 1 ELSE 0 END AS InstanceEarliestRecord -- flags whether the earliest record returned had a stoppage
        FROM [dbo].[Raw]
        WHERE [LineId] = @LineId
            AND [DateTime] BETWEEN @From AND @To 
        ) AS a
        WHERE a.InstanceEarliestRecord + a.InstanceStart + a.InstanceEnd > 0
    GROUP BY
        a.[LineId]
        ,a.[DateTime]
        ,a.[Stoppage]
        ,a.[StoppageReasonCategory]
        ,a.[StoppageReasonCode]
        ,a.Lead_Stoppage -- this is only used for dev/test
        ,a.InstanceStart
        ,a.InstanceEnd
        ,a.EarliestRecord -- flag the earliest record returned
        ,a.InstanceEarliestRecord -- flags whether the earliest record returned had a stoppage
    ) AS b
    GROUP BY
        b.[LineId]
        ,b.[StoppageReasonCategory]
        ,b.[StoppageReasonCode]
    ORDER BY
        [Duration] DESC
END
GO